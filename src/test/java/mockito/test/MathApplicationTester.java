package mockito.test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import mockito.test.example2.CalculatorService;
import mockito.test.example2.MathApplication;

@SuppressWarnings("deprecation")
@RunWith(MockitoJUnitRunner.class)
public class MathApplicationTester {

	@InjectMocks
	MathApplication mathApplication = new MathApplication();

	@Mock
	CalculatorService calcService;

	@Before
	public void init() {
		this.mathApplication.setCalculatorService(calcService);
	}

	@Test
	public void testAdd() {
		when(calcService.add(10.0, 20.0)).thenReturn(30.00);

		Assert.assertEquals(mathApplication.add(10.0, 20.0), 30.0, 0);

		System.out.println(verify(calcService).add(10.0, 20.0));
	}

	@Test
	public void testSubtract() {
		when(calcService.subtract(10.0, 20.0)).thenReturn(-10.00);

		Assert.assertEquals(mathApplication.subtract(10.0, 20.0), -10.0, 0);

		System.out.println(verify(calcService).subtract(10.0, 20.0));
	}

	@Test
	public void testmultiply() {
		when(calcService.multiply(10.0, 20.0)).thenReturn(30.00);

		Assert.assertEquals(mathApplication.multiply(10.0, 20.0), 30.0, 0);

		System.out.println(verify(calcService).multiply(10.0, 20.0));
	}

	@Test
	public void testdivide() {
		when(calcService.divide(10.0, 20.0)).thenReturn(30.00);

		Assert.assertEquals(mathApplication.divide(10.0, 20.0), 30.0, 0);

		System.out.println(verify(calcService).divide(10.0, 20.0));
	}

}
