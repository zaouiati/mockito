package mockito.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import mockito.test.example1.Portfolio;
import mockito.test.example1.Stock;
import mockito.test.example1.StockService;

public class PortfolioTester {

	Portfolio portfolio;
	StockService stockService;

	@Before
	public void setUp() {

		portfolio = new Portfolio();

		stockService = mock(StockService.class);

		portfolio.setStockService(stockService);
	}

	@Test
	public void testMarketValue() {

		List<Stock> stocks = new ArrayList<Stock>();
		Stock googleStock = new Stock("1", "Google", 10);
		Stock microsoftStock = new Stock("2", "Microsoft", 100);

		stocks.add(googleStock);
		stocks.add(microsoftStock);

		portfolio.setStocks(stocks);

		double marketValue = portfolio.getMarketValue();
		assertTrue(true);
	}
	

	@Test
	public void testMarketGetters() {

		List<Stock> stocks = new ArrayList<Stock>();
		Stock googleStock = new Stock("1", "Google", 10);
		Stock microsoftStock = new Stock("2", "Microsoft", 100);

		stocks.add(googleStock);
		stocks.add(microsoftStock);
		portfolio.setStocks(stocks);
		
		portfolio.getStockService();
		portfolio.getStocks();

		assertTrue(true);
	}

	@Test
	public void testStockSettersGetters() {

		Stock googleStock = new Stock("1", "Google", 10);

		googleStock.setStockId("2");
		
		String stockId = googleStock.getStockId();
		
		assertEquals(stockId, "2");
		
		String ticket = googleStock.getTicker();
		
		assertEquals(ticket, "Google");
	}
}
