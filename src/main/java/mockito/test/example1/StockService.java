package mockito.test.example1;

public interface StockService {
	
	   public double getPrice(Stock stock);
}