@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "vehicles")
public class Vehicle {

	@Id
	@Column(name = "VIN", nullable = false, length = 17)
	@NonNull
	private String vin;

	@Column(name = "make", nullable = false)
	@NonNull
	@NotEmpty(message = "'make' field was empty")
	private String make;

	@Column(name = "model", nullable = false)
	@NonNull
	@NotEmpty(message = "model' field was empty")
	private String model;

	@Column(name = "year", nullable = false)
	@NonNull
	// Fun fact: VINs were first used until 1954 in the United States
	@DecimalMin(value = "1954", message = "VINs before 1954 are not accepted")
	private Integer year;

	@Column(name = "is_older", nullable = true)
	private Boolean is_older;
}

-------------------------

@RestController
@RequestMapping("/demo")
public class VehicleController {

	@Autowired
	VehicleService vehicleService;

	@ApiOperation(value = "Retrieves a list of all vehicle records")
	@GetMapping(value = "/vehicles", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Vehicle>> getAllVehicles() {

		List<Vehicle> vehicles = vehicleService.getAllVehicles();
		if (vehicles.isEmpty()) {
			throw new VehicleNotFoundException("No vehicle records were found");
		}
		return new ResponseEntity<List<Vehicle>>(vehicles, HttpStatus.OK);
	}

------------------

@Service
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	VehicleRepository vehicleRepository;

	@Override
	public List<Vehicle> getAllVehicles() {
		return vehicleRepository.findAll();
	}
/*** Implementation for all service methods... ****/

// Special case buisness logic
private static void validateVehicleHelper(Vehicle vehicle) {
  int vinLength = vehicle.getVin().length();
  /*
   * Fun Fact: Prior to 1981, VINs varied in length from 11 to 17 characters. Auto
   * checking on vehicles older than 1981 can resulted in limited info.
   */
  if (vinLength > 17 || vinLength < 11) {
    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "VIN is an invalid length");
  }
  if (vinLength < 17 && vehicle.getYear() >= 1981) {
    throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "VIN length is invalid for the declared year");
  }
}

----------------
/*
 * DAO methods interface. Just by extending the JpaRepository
 * we can use the concrete implementations of the most relevant query methods.
 * For example 'findById' or 'findAll'
 * Spring Data JPA uses naming conventions and reflection to generate the
 * concrete implementation of the interface we define.
 */
@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, String> {

}
-----------------

- Create 8 vehicle records for testing
INSERT INTO vehicles (VIN, make, model, year, is_older) VALUES
  ('FR45212A24D4SED66', 'Ford', 'F-150', 2010, false),
  ('FR4EDED2150RFT5GE', 'Ford', 'Ranger', 1992, null),
  ('XDFR64AE9F3A5R78S', 'Chevrolet', 'Silverado 2500', 2017, false),
  ('XDFR6545DF3A5R896', 'Toyota', 'Tacoma', 2008, null),
  ('GMDE65A5ED66ER002', 'GMC', 'Sierra', 2012, false),
  ('PQERS2A36458E98CD', 'Nissan', 'Titan', 2013, false),
  ('194678S400005', 'Chevrolet', 'Corvette', 1977, true),
  ('48955460210', 'Ford', 'Mustang', 1974, true);
-----------------------


@WebMvcTest(VehicleController.class)
@ActiveProfiles("test")
public class DemoWebLayerTest {

	/*
	 * We can @Autowire MockMvc because the WebApplicationContext provides an
	 * instance/bean for us
	 */
	@Autowired
	MockMvc mockMvc;

	/*
	 * Jackson mapper for Object -> JSON conversion
	 */
	@Autowired
	ObjectMapper mapper;

	/*
	 * We use @MockBean because the WebApplicationContext does not provide
	 * any @Component, @Service or @Repository beans instance/bean of this service
	 * in its context. It only loads the beans solely required for testing the
	 * controller.
	 */
	@MockBean
	VehicleService vechicleService;

	@Test
	public void get_allVehicles_returnsOkWithListOfVehicles() throws Exception {

		List<Vehicle> vehicleList = new ArrayList<>();
		Vehicle vehicle1 = new Vehicle("AD23E5R98EFT3SL00", "Ford", "Fiesta", 2016, false);
		Vehicle vehicle2 = new Vehicle("O90DEPADE564W4W83", "Volkswagen", "Jetta", 2016, false);
		vehicleList.add(vehicle1);
		vehicleList.add(vehicle2);

		// Mocking out the vehicle service
		Mockito.when(vechicleService.getAllVehicles()).thenReturn(vehicleList);

		mockMvc.perform(MockMvcRequestBuilders.get("/demo/vehicles").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].vin", is("AD23E5R98EFT3SL00"))).andExpect(jsonPath("$[0].make", is("Ford")))
				.andExpect(jsonPath("$[1].vin", is("O90DEPADE564W4W83")))
				.andExpect(jsonPath("$[1].make", is("Volkswagen")));
	}

	@Test
	public void post_createsNewVehicle_andReturnsObjWith201() throws Exception {
		Vehicle vehicle = new Vehicle("AD23E5R98EFT3SL00", "Ford", "Fiesta", 2016, false);

		Mockito.when(vechicleService.createVehicle(Mockito.any(Vehicle.class))).thenReturn(vehicle);

		// Build post request with vehicle object payload
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/demo/create/vehicle")
				.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
				.characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(vehicle));

		mockMvc.perform(builder).andExpect(status().isCreated()).andExpect(jsonPath("$.vin", is("AD23E5R98EFT3SL00")))
				.andExpect(MockMvcResultMatchers.content().string(this.mapper.writeValueAsString(vehicle)));
	}

	@Test
	public void post_submitsInvalidVehicle_WithEmptyMake_Returns400() throws Exception {
		// Create new vehicle with empty 'make' field
		Vehicle vehicle = new Vehicle("AD23E5R98EFT3SL00", "", "Firebird", 1982, false);

		String vehicleJsonString = this.mapper.writeValueAsString(vehicle);

		ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/demo/create/vehicle/")
				.contentType(MediaType.APPLICATION_JSON).content(vehicleJsonString)).andExpect(status().isBadRequest());

		// @Valid annotation in controller will cause exception to be thrown
		assertEquals(MethodArgumentNotValidException.class,
				resultActions.andReturn().getResolvedException().getClass());
		assertTrue(resultActions.andReturn().getResolvedException().getMessage().contains("'make' field was empty"));
	}

	@Test
	public void put_updatesAndReturnsUpdatedObjWith202() throws Exception {
		Vehicle vehicle = new Vehicle("AD23E5R98EFT3SL00", "Ford", "Fiesta", 2016, false);

		Mockito.when(vechicleService.updateVehicle("AD23E5R98EFT3SL00", vehicle)).thenReturn(vehicle);

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
				.put("/demo/update/vehicle/AD23E5R98EFT3SL00", vehicle).contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON).characterEncoding("UTF-8")
				.content(this.mapper.writeValueAsBytes(vehicle));

		mockMvc.perform(builder).andExpect(status().isAccepted()).andExpect(jsonPath("$.vin", is("AD23E5R98EFT3SL00")))
				.andExpect(MockMvcResultMatchers.content().string(this.mapper.writeValueAsString(vehicle)));
	}

	@Test
	public void delete_deleteVehicle_Returns204Status() throws Exception {
		String vehicleVin = "AD23E5R98EFT3SL00";

		VehicleService serviceSpy = Mockito.spy(vechicleService);
		Mockito.doNothing().when(serviceSpy).deleteVehicle(vehicleVin);

		mockMvc.perform(MockMvcRequestBuilders.delete("/demo/vehicles/AD23E5R98EFT3SL00")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent());

		verify(vechicleService, times(1)).deleteVehicle(vehicleVin);
	}

	@Test
	public void get_vehicleByVin_ThrowsVehicleNotFoundException() throws Exception {

		// Return an empty Optional object since we didn't find the vin
		Mockito.when(vechicleService.getVehicleByVin("AD23E5R98EFT3SL00")).thenReturn(Optional.empty());

		ResultActions resultActions = mockMvc.perform(
				MockMvcRequestBuilders.get("/demo/vehicles/AD23E5R98EFT3SL00").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());

		assertEquals(VehicleNotFoundException.class, resultActions.andReturn().getResolvedException().getClass());
		assertTrue(resultActions.andReturn().getResolvedException().getMessage()
				.contains("Vehicle with VIN (" + "AD23E5R98EFT3SL00" + ") not found!"));
	}
}


------------

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = SpringTestApplication.class)
@TestInstance(Lifecycle.PER_CLASS)
@ActiveProfiles({ "integration" })
public class AppRestAssuredIT {

	@Value("${local.server.port}")
	private int ports;

	@BeforeAll
	public void setUp() {
		port = ports;
		baseURI = "http://localhost/demo"; // Will result in "http://localhost:xxxx/demo"
	}

	@Test
	public void get_AllVehicles_returnsAllVehicles_200() {

		String vinArray[] = { "FR45212A24D4SED66", "FR4EDED2150RFT5GE", "XDFR6545DF3A5R896", "GMDE65A5ED66ER002",
				"PQERS2A36458E98CD", "194678S400005", "48955460210" };

		ValidatableResponse response = given().contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).when().get("/vehicles").then();

		System.out.println("'getAllVehicles_returnsAllVehicles_200()' response:\n" + response.extract().asString());

		response.assertThat().statusCode(HttpStatus.OK.value()).body("content.size()", greaterThan(7))
				.body(containsString("Corvette")).body("find {it.vin == 'FR45212A24D4SED66'}.year", equalTo(2010))
				.body("find {it.vin == 'FR45212A24D4SED66'}.year", equalTo(2010))
				.body("make", hasItems("Ford", "Chevrolet", "Toyota", "Nissan")).body("make", not(hasItem("Honda")))
				.body("vin", hasItems(vinArray)).body("findAll {it.year < 1990}.size()", is(2));
	}

	@Test
	public void get_VehicleById_returnsVehicle_200() {

		ValidatableResponse response = given().contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).when().get("/vehicles/FR45212A24D4SED66").then();

		System.out.println("'getVehicleById_returnsVehicle_200()' response:\n" + response.extract().asString());

		response.assertThat().statusCode(HttpStatus.OK.value()).body("make", equalTo("Ford"))
				.body("model", equalTo("F-150")).body("year", equalTo(2010)).body("is_older", equalTo(false));
	}

	@Test
	public void get_VehicleById_returnsNotFound_404() {

		ValidatableResponse response = given().contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).when().get("/vehicles/NON-EXISTING-ID77").then();

		System.out.println("'get_VehicleById_returnsNotFound_404()' response:\n" + response.extract().asString());

		response.assertThat().statusCode(HttpStatus.NOT_FOUND.value()).body("errorMessage",
				containsString("404 Vehicle with VIN (NON-EXISTING-ID77) not found"));
	}

	@Test
	public void post_newVehicle_returnsCreatedVehicle_201() {
		// Build new vehicle to post
		Vehicle newVehicle = Vehicle.builder().vin("X0RF654S54A65E66E").make("Toyota").model("Supra").year(2020)
				.is_older(false).build();

		ValidatableResponse response = given().contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).body(newVehicle).when().post("/create/vehicle").then();

		System.out.println("'post_Vehicle_returnsCreatedVehicle_201()' response:\n" + response.extract().asString());

		response.assertThat().statusCode(HttpStatus.CREATED.value()).body("vin", equalTo("X0RF654S54A65E66E"))
				.body("make", equalTo("Toyota")).body("model", equalTo("Supra")).body("year", equalTo(2020))
				.body("is_older", equalTo(false));
	}

	@Test
	public void post_newVehicle_Returns_BadRequest_400() {
		// Create new vehicle with a bad VIN length for the declared model year
		Vehicle newVehicle = Vehicle.builder().vin("BAD-LENGTH-VIN").make("Chevrolet").model("Camaro").year(2018)
				.is_older(false).build();

		ValidatableResponse response = given().contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).body(newVehicle).when().post("/create/vehicle").then();

		System.out.println("'post_newVehicle_Returns_BadRequest_400()' response:\n" + response.extract().asString());

		response.assertThat().statusCode(HttpStatus.BAD_REQUEST.value()).body("errorMessage",
				containsString("VIN length is invalid for the declared year"));
	}

	@Test
	public void put_updateVehicle_returnsUpdatedVehicle_202() {
		// Update the year on the vehicle 1992 -> 1997
		Vehicle updateVehicle = Vehicle.builder().vin("FR4EDED2150RFT5GE").make("Ford").model("Ranger").year(1997)
				.is_older(false).build();

		ValidatableResponse response = given().contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).body(updateVehicle).when()
				.put("/update/vehicle/FR4EDED2150RFT5GE").then();

		System.out
				.println("'put_updateVehicle_returnsUpdatedVehicle_202()' response:\n" + response.extract().asString());

		response.assertThat().statusCode(HttpStatus.ACCEPTED.value()).body("vin", equalTo("FR4EDED2150RFT5GE"))
				.body("make", equalTo("Ford")).body("model", equalTo("Ranger")).body("year", equalTo(1997))
				.body("is_older", equalTo(false));
	}

	@Test
	public void delete_vehicle_returnsNoContent_204() {

		ValidatableResponse response = given().contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).when().delete("/vehicles/XDFR64AE9F3A5R78S").then();

		System.out.println("'delete_vehicle_returnsNoContent_204()' response:\n" + response.extract().asString());

		response.assertThat().statusCode(HttpStatus.NO_CONTENT.value());
	}

}



------------------------
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT, classes = SpringTestApplication.class)
@ActiveProfiles("integration")
public class AppTestRestTemplateIT {

	final private static int port = 8080;
	final private static String baseUrl = "http://localhost:";

	/*
	 * @SpringBootTest registers a TestRestTeplate bean so we can directly @Autowire
	 */
	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private VehicleRepository vehicleRepository;

	@Test
	public void get_allVehicles_ReturnsAllVehicles_OK() {

		List<String> expectedVINList = Stream.of("FR45212A24D4SED66", "FR4EDED2150RFT5GE", "XDFR6545DF3A5R896",
				"XDFR64AE9F3A5R78S", "PQERS2A36458E98CD", "194678S400005", "48955460210").collect(Collectors.toList());

		ResponseEntity<List<Vehicle>> responseEntity = this.restTemplate.exchange(baseUrl + port + "/demo/vehicles",
				HttpMethod.GET, null, new ParameterizedTypeReference<List<Vehicle>>() {
				});

		List<Vehicle> vehiclesResponseList = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertTrue(vehiclesResponseList.size() > 7);
		assertTrue(vehiclesResponseList.stream().anyMatch((vehicle) -> {
			return expectedVINList.contains(vehicle.getVin());
		}));
	}

	@Test
	public void get_vehicleById_Returns_Vehicle_OK() {

		ResponseEntity<Vehicle> responseEntity = this.restTemplate
				.getForEntity(baseUrl + port + "/demo/vehicles/48955460210", Vehicle.class);

		Vehicle expectedVehicle = Vehicle.builder().vin("48955460210").make("Ford").model("Mustang").year(1974)
				.is_older(true).build();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(expectedVehicle, responseEntity.getBody());
	}

	@Test
	public void get_vehicleById_Returns_NotFound_404() {

		// We are expecting an string error message in JSON
		ResponseEntity<String> result = this.restTemplate.exchange(baseUrl + port + "/demo/vehicles/MISSING-VIN123456",
				HttpMethod.GET, null, String.class);

		// Parse JSON message response
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonTree = null;
		try {
			jsonTree = mapper.readTree(result.getBody());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		JsonNode jsonNode = jsonTree.get("errorMessage");

		assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
		// Assert the proper error message is received
		assertTrue(jsonNode.asText().contains("404 Vehicle with VIN (MISSING-VIN123456) not found"));
	}

	@Test
	public void post_createNewVehicle_Returns_201_Created() {

		// Create a new vehicle
		Vehicle newVehicle = Vehicle.builder().vin("X0RF654S54A65E66E").make("Toyota").model("Supra").year(2020)
				.is_older(false).build();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Vehicle> request = new HttpEntity<Vehicle>(newVehicle, headers);

		ResponseEntity<Vehicle> responseEntity = this.restTemplate
				.postForEntity(baseUrl + port + "/demo/create/vehicle", request, Vehicle.class);

		// Post request should return the newly created entity back to the client
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals("X0RF654S54A65E66E", responseEntity.getBody().getVin());
		assertEquals("Toyota", responseEntity.getBody().getMake());
		assertEquals("Supra", responseEntity.getBody().getModel());
		assertFalse(responseEntity.getBody().getIs_older());

		// Double check this new vehicle has been stored in our embedded H2 db
		Optional<Vehicle> op = vehicleRepository.findById("X0RF654S54A65E66E");
		assertTrue(op.isPresent());
		assertEquals("X0RF654S54A65E66E", op.get().getVin());
	}

	@Test
	public void post_createNewVehicle_Returns_400_BadRequest() {

		ResponseEntity<String> result = null;

		// Create new vehicle with a bad VIN length for the declared model year
		Vehicle newVehicle = Vehicle.builder().vin("BAD-LENGTH-VIN").make("Chevrolet").model("Camaro").year(2018)
				.is_older(false).build();

		// We'll use an object mapper to show our HttpEntity also accepts JSON string
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;
		// Our post consumes JSON format
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		try {
			String vehicleJSONString = mapper.writeValueAsString(newVehicle);

			HttpEntity<String> request = new HttpEntity<String>(vehicleJSONString, headers);
			result = this.restTemplate.postForEntity(baseUrl + port + "/demo/create/vehicle", request, String.class);
			// Our JSON error message has an "errorMessage" attribute
			jsonNode = mapper.readTree(result.getBody()).get("errorMessage");

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
		// Assert the expected error message
		assertTrue(jsonNode.asText().contains("VIN length is invalid for the declared year"));
	}

	@Test
	public void put_updateVehicle_Returns_202_Accepted() {

		// Update vehicle. Need to update to the correct year '1992' -> '1996'
		Vehicle vehicleUpdate = Vehicle.builder().vin("FR4EDED2150RFT5GE").make("Ford").model("Ranger").year(1996)
				.is_older(false).build();

		// Our targeted URI consumes JSON format
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Vehicle> requestEntity = new HttpEntity<Vehicle>(vehicleUpdate, headers);

		ResponseEntity<Vehicle> responseEntity = this.restTemplate.exchange(
				baseUrl + port + "/demo/update/vehicle/FR4EDED2150RFT5GE", HttpMethod.PUT, requestEntity,
				Vehicle.class);

		// Put request should return the updated vehicle entity back to the client
		assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
		assertEquals(vehicleUpdate, responseEntity.getBody());
	}

	@Test
	public void delete_vehicleById_Returns_NoContent_204() {

		ResponseEntity<Object> responseEntity = this.restTemplate
				.exchange(baseUrl + port + "/demo/vehicles/GMDE65A5ED66ER002", HttpMethod.DELETE, null, Object.class);

		assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
		assertNull(responseEntity.getBody());

		// Double check the vehicle has been deleted from our embedded H2 db
		Optional<Vehicle> optional = vehicleRepository.findById("GMDE65A5ED66ER002");
		assertFalse(optional.isPresent());
	}
}








-------------------------------- Other example using spring boot MockMvc and Mockito --------------------------------------
@RunWith(SpringRunner.class)
@WebMvcTest(value = EmployeeController.class, secure = false)
public class EmployeeController {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    Course mockProject = new Course("Project1", "Spring-Boot Project", "Data Management",Arrays.asList("Spring-Boot", "AngularJS", "MongoDb","Java"));

    @Test
    public void testRetrieveProjecteDetails() throws Exception {

        Mockito.when(employeeService.retrieveProjecteDetails(Mockito.anyString(),Mockito.anyString())).thenReturn(mockProject);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/employee/Emp1/project/Project1")
                .accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse());

        String expected = "{id:Project1,name:Spring-Boot Project,description:Data Management}";

        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }
}


--------------------------
package com.example.joy.myFirstSpringBoot.controllers;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.joy.myFirstSpringBoot.services.BasicBirthdayService;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {BirthdayInfoController.class, BasicBirthdayService.class})
@WebMvcTest
class BirthdayInfoControllerIT {
    private final static String TEST_USER_ID = "user-id-123";
    String bd1 = LocalDate.of(1979, 7, 14).format(DateTimeFormatter.ISO_DATE);
    String bd2 = LocalDate.of(2018, 1, 23).format(DateTimeFormatter.ISO_DATE);
    String bd3 = LocalDate.of(1972, 3, 17).format(DateTimeFormatter.ISO_DATE);
    String bd4 = LocalDate.of(1945, 12, 2).format(DateTimeFormatter.ISO_DATE);
    String bd5 = LocalDate.of(2003, 8, 4).format(DateTimeFormatter.ISO_DATE);

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetBirthdayDOW() throws Exception {
        testDOW(bd1, "SATURDAY");
        testDOW(bd2, "TUESDAY");
        testDOW(bd3, "FRIDAY");
        testDOW(bd4, "SUNDAY");
        testDOW(bd5, "MONDAY");
    }

    @Test
    public void testGetBirthdayChineseSign() throws Exception {
        testZodiak(bd1, "Sheep");
        testZodiak(bd2, "Dog");
        testZodiak(bd3, "Rat");
        testZodiak(bd4, "Rooster");
        testZodiak(bd5, "Sheep");
    }

    @Test
    public void testGetBirthdaytestStarSign() throws Exception {
        testStarSign(bd1, "Cancer");
        testStarSign(bd2, "Aquarius");
        testStarSign(bd3, "Pisces");
        testStarSign(bd4, "Sagittarius");
        testStarSign(bd5, "Leo");
    }

    private void testDOW(String birthday, String dow) throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/birthday/dayOfWeek")
                .with(user(TEST_USER_ID))
                .with(csrf())
                .content(birthday)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String resultDOW = result.getResponse().getContentAsString();
        assertNotNull(resultDOW);
        assertEquals(dow, resultDOW);
    }

    private void testZodiak(String birthday, String czs) throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/birthday/chineseZodiac")
                .with(user(TEST_USER_ID))
                .with(csrf())
                .content(birthday)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String resultCZ = result.getResponse().getContentAsString();
        assertNotNull(resultCZ);
        assertEquals(czs, resultCZ);
    }

    private void testStarSign(String birthday, String ss) throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/birthday/starSign")
                .with(user(TEST_USER_ID))
                .with(csrf())
                .content(birthday)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String resultSS = result.getResponse().getContentAsString();
        assertNotNull(resultSS);
        assertEquals(ss, resultSS);
    }
}